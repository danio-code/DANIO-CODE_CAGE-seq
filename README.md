<h1>DANIO-CODE CAGE-seq pipeline</h1>


This repository contains the pipeline in two different versions: 
<ul>
<li>a DNAnexus independent version, which can be run on your computer ("<a href="https://gitlab.com/danio-code/DANIO-CODE_CAGESeq/tree/master/script_dong/DNAnexus_apps">/script_dong/DNAnexus_apps</a>"), </li>
<li>and the DNAnexus application version ("<a href="https://gitlab.com/danio-code/DANIO-CODE_CAGESeq/tree/master/script_dong/indep_pipeline">/script_dong/DNAnexus_apps</a>").</li>
</ul>

Fore more information about how to use the DNAnexus platform, please follow the link below:<br>
<a href="https://wiki.dnanexus.com/UI/Quickstart">https://wiki.dnanexus.com/UI/Quickstart</a>

<h2>Updates</h2>

<ul>
  <li>CAGE-seq processing workflow</li>
</ul>

<h3>v1.6.0</h3>
<ul>
  <li>Replaced DNAnexus app output SAM file from mapping step (loop.sh) to BAM file</li>
</ul>
<h3>v1.5.1</h3>
<ul>
  <li>Corrected the zenbufile output to match bigWig format</li>
  <li>Added 1st G trimming step (v1.5.0)</li>
</ul>
<h3>v1.3.0</h3>
<ul>
  <li>Adapted raw script to DNAnexus application</li>
  <li>Can now take multiple files as inputs : merged three steps into one single script.</li>
</ul>
<h3>v1.1.0</h3>
<ul>
  <li>Added the tag counting step (bedIctss.sh).</li>
</ul>
<h3>v1.0.0</h3>
<ul>
  <li>Initial release. Can run on only one sample at a time.</li>
</ul>

<h2>How to run the pipelines</h2>
<h3>DNAnexus indepndent scripts</h3>

<h4>Dependencies (to install, prior to running the pipeline):</h4>
*   bash
*   fastx
*   Bowtie
*   samtools
*   bedtools

<h4>Inputs: </h4>
*   A bowtie index of the genome used for mapping (bowtie-index)
*   Reads to be mapped, in fastq format

<h4>Installation:</h4>

*   clone the following repository: `$ git clone git@gitlab.com:danio-code/DANIO-CODE_CAGESeq/tree/master/script_dong/indep_pipeline/dcc.git`
*   check if all dependencies listed have been installed in your system
*   Put all the reads in fastq.gz format you want to process in this folder : `/indep_pipeline/test_data/input`
*   check parameters of each script in the src directory. Change the filepaths if needed.

<h4>Run pipeline:</h4>

*   move to main directory: `$ cd /script_dong/indep_pipeline/`
*   run the main script: `$ ./main.sh`

<h3>DNAnexus applications</h3>

To run the pipeline on the DNAnexus platform it is neccessary to build up 
applets on the platform. That could be done by running the script build_applets
in the dna-me-pipeline/dnanexus/ folder (dx-toolkit required).

The repository contains folders that correspond to the different applets used for the project.

Align reads with Bowtie: loop_v1.3
Concatenation of the zenbu format results: ctsstoctssall
Tag clustering: ctsstoparaclu
Counts per tag cluster: bedictss

<p>
  <span>The repository contains each applet built and used for the workflow on DNAnexus. Each applet corresponds to one step of the pipeline.</span>
  <span>To learn more about how to use and run the script on the DNAnexus platform, you can also follow the tutorial: <a href='https://wiki.dnanexus.com/Home'>link</a> </span>
  <span>NB : The bowtie genome index file must be built by running the bowtie indexer app. Use the output as first input for the workflow.</span>
</p>

<h4>To run the pipeline via DNAnexus:</h4>
*   Create an account on DNAnexus (if you don't have one yet)
*   Download and install dx-toolkit from DNAnexus
*   Build the following apps contained in the repository with the `$ dx build --create-applet` command from the dx-toolkit
*   Connect and build up the workflow below on the DNAnexus platform using the different apps
*   Run the workflow after entering all the corresponding inputs.

NB: The flowchart below indicates the way the applets should be assembled in the workflow: 

```

Workflow - CAGE-seq pipeline v1.6

Inputs                             -->            App            -->        Outputs
==============================================================================================
|Raw reads                           | LOOP                        | bowtie_bam              |
|Indexed converted genome (bowtie)   | version: 1.3.0              | bedfiles                |
|                                    | Default parameters          | zenbufiles (*)          |
|                                    |                             | data_ids                |
==============================================================================================
|zenbufiles (*)                      | CTSSTOCTSSALL               | ctssall (*)             |
|                                    | version: 1.1.0              |                         |        
|                                    | Default parameters          |                         |    
==============================================================================================                        
|ctssall (*)                         | CTSSTOPARACLU               | ctss_pos_4P             |
|                                    | version: 1.1.0              | ctss_neg_4P             |        
|                                    | Default parameters          | ctss_pos_4Ps            |    
|                                    |                             | ctss_neg_4Ps            |
|                                    |                             | par_pos_30              |
|                                    |                             | par_neg_30              |
|                                    |                             | par_pos_30_2_200        |
|                                    |                             | par_neg_30_2_200        |
|                                    |                             | par_30_2_200            |
|                                    |                             | par_30_2_200_bed (*)    |
==============================================================================================
|zenbufiles (*)                      | BEDICTSS                    | counts                  |
|par_30_2_200_bed (*)                | version: 1.2.0              |                         |        
|                                    | Default parameters          |                         |    
==============================================================================================          

```
NOTE: Outputs and Inputs with asterisk represent connections between Apps
NOTE: Only DNAnexus output names are listed in the outputs.


<h2>Description of the pipeline</h2>

<h4>Steps</h4>

<p>
  <span>The current CAGE-seq processing pipeline ("CAGE_pipeline_v1.6") includes different processing steps :</span>
</p>

<h5>Genome indexing for mapping (optional) + Mapping to the reference genome (Bowtie)</h5>
<p>
For mapping, we chose to use the Bowtie, as it is very efficient for aligning large sets of short DNA sequences (reads) on large genomes such as CAGE-seq data. 
As input it takes a list of the CAGE-seq samples, as well as the bowtie index obtained from the reference genome.
The Bowtie package contains a function to produce this index from your reference genome if it has not been created yet. (bowtie-index).
Output gives the list of reads mapped on the genome, with the different coordinates and mapping scores.
For more information about Bowtie: <a href="http://bowtie-bio.sourceforge.net/manual.shtml#default-bowtie-output">Link to Bowtie manual</a>
</p>

<h5>File format conversion (samtools/bedtools)</h5>
After mapping we modified the BED output from Bowtie so that it could match the ZENBU genome browser platform data upload format.
For more informtion about Zenbu: <a href="https://zenbu-wiki.gsc.riken.jp/zenbu/wiki/index.php/Main_Page">https://zenbu-wiki.gsc.riken.jp/zenbu/wiki/</a>

<h5>Tag clustering (Paraclu-9)</h5>
<p>
For the clustering step we decided to use Paraclu-9 (version 9.1). One of the reasons is that the paraclu clustering tool can find clusters within clusters.
As output, the clustering step produces lists of the different clusters identified.
For more information about Paraclu-9: <a href="http://cbrc3.cbrc.jp/~martin/paraclu/README.txt">http://cbrc3.cbrc.jp/~martin/paraclu/README.txt</a>
</p>


<p>
  <span>Until now, the only CAGE-data series available on the DCC (Mueller Lab) has been processed for the freeze.</span>
  <span>The current version has specifically been adapted for this dataset and might not process other types of CAGE-seq data obtained with other protocols such as nAnTi-CAGE or nanoCAGE ; It only processes samples with reads length of 27 bp and for which the first G is still present ; the usual CAGE protocol has a known bias with a nonspecific guanine (G) at the most 5′ end of the CAGE tags, which is attributed to the template-free 5′-extension during the first-strand cDNA synthesis. However, for the most recent methods, they are sometimes removed to ensure a better alignment quality.</span>
  <span>We also added an additional fastQC step in our DNAnexus workflow. It has been removed there but you can manually add it to the script.</span>
</p>


<h4>Flow of the pipeline</h4>

```
INPUTS:  reads.fq.gz           *.bowtiemapping.bam   *.VS.genome.bed          *.VS.genome.4zenbu.bed  ctssall.bed          *.VS.genome.4zenbu.bed
         bowtie_index               |                 |                        |                       |                   *par_30_2_200_bed.bed
            |                       |                 |                        |                       |                     |
            |                       |                 |                        |                       |                     |
            V                       V                 V                        V                       V                     V
STEPS: ( SCR_DR_mapping_v0.sh,   sam_To_BED.sh,      BedToZenbu.sh )  ======> ctssToCtssAll.sh =====> ctssToParaclu.sh ===> bedIctss.sh
            |                       |                 |                        |                       |                     |
            |                       |                 |                        |                       |                     |
            |                       |                 |                        |                       |                     |
            V                       V                 V                        V                       V                     V
OUTPUTS: *.bowtiemapping.bam   *.VS.genome.bed       *.VS.genome.4zenbu.bed   ctssall.bed           *ctss_pos_4P.bed       *counts.tsv
																								    *ctss_neg_4P.bed
																								    *ctss_pos_4Ps.bed
																								    *ctss_neg_4Ps.bed
																								    *par_pos_30.bed
																								    *par_neg_30.bed
																								    *par_pos_30_2_200.bed
																								    *par_neg_30_2_200.bed																									  
																								    *par_30_2_200.bed
																								    *par_30_2_200.bed.bed	
```

<p>
Note: the "loop" applet for the DNAnexus pipeline version regroups the three first steps: 
SCR_DR_mapping_v0.sh, sam_To_BED.sh and BedToZenbu.sh
</p>

<h3>Outputs</h3>

<p>
  <span>For each sample, are available:</span>
</p>
<ul style="list-style-type: square;">
  <li>
    <span>The fastQC quality control results of the sample, in txt (“*.stats-fastqc.txt”) and html format (“*.stats-fastqc.html”)</span><br>
    For more information about FastQC output: <a href="https://biof-edu.colorado.edu/videos/dowell-short-read-class/day-4/fastqc-manual">https://biof-edu.colorado.edu/videos/dowell-short-read-class/day-4/fastqc-manual</a>
    <br>
  </li>
  <br>
  <li>
    <span>The outputs from the Bowtie mapping:</span>
    <ul style="list-style-type: square;">
      <li>
        <span>Raw output in BAM format (“*.bowtiemapping.bam”):</span>
        <p>
        Alignment results in BAM format.<br>
        For more information about the BAM/SAM format: <a href="https://samtools.github.io/hts-specs/SAMv1.pdf">https://samtools.github.io/hts-specs/SAMv1.pdf<a><br>
        </p>
      </li>
      <li>
        BED6 format of the results after quality control, sorting and indexing (“*.VS.genome.bed”)<br>
        Quality control parameters: -b -q 20<br>
        Alignment results reformatted into specific BED format.<br>
        <ul>
        <li>Column 1: Chromosome name/number </li>
        <li>Column 2: Starting position of the alignment in the chromosome </li> 
        <li>Column 3: Ending position of the alignment in the chromosome </li>
        <li>Column 4: Name of the BED line </li>
        <li>Column 5: Score between 1 and 1000. For more details: <a href="https://genome.ucsc.edu/FAQ/FAQformat.html#format1">BED format</a> </li>
        <li>Column 6: Strand </li>
        </ul>
      </li>
      <li>
        <span>BED6 format, adapted for ZENBU genome browser platform (“*.VS.genome.4zenbu.bed”)</span>
        <ul>
        <li>Column 1: Chromosome name/number </li>
        <li>Column 2: Starting position of the alignment in the chromosome </li>
        <li>Column 3: Ending position of the alignment in the chromosome (NB: ZENBU uses a 1-based-inclusive coordinate space.) </li>
        <li>Column 4: Name formatted for Zenbu browser </li>
        <li>Column 5: Score between 1 and 1000. For more details: <a href="https://genome.ucsc.edu/FAQ/FAQformat.html#format1">BED format</a> </li>
        <li>Column 6: Strand </li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
<p>
  <span>And for each set (series):</span>
</p>
<ul style="list-style-type: square;">
  <li>
    <span>A list of all the CAGE defined Transcriptional Start Sites (CTSS) ("ctssall.bed"): all zenbu files concatenated into one file</span>
    <ul style="list-style-type: square;">
      <li>
        <span>After quality filtering and conversion to Paraclu clustering input format : positive strand ("*ctss_pos_4P.bed") and negative strand ("*ctss_neg_4P.bed ")</span>
      </li>
      <li>
        <span>with sorted results: positive strand ("*ctss_pos_4Ps.bed") and negative strand ("*ctss_neg_4Ps.bed ")</span>
        <ul>
          <li>Column 1: Chromosome name</li>
          <li>Column 2: Strand</li>
          <li>Column 3: Cluster position</li>
          <li>Column 4: Number of reads aligned</li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
<ul style="list-style-type: square;">
  <li>
    <span>A list of the tag cluster genomic regions in BED file format:</span>
    <ul style="list-style-type: square;">
      <li>
        <span>Paraclu clustering raw outputs (counts threshold = 30): positive strand ("*par_pos_30.bed") and negative strand ("*par_neg_30.bed")</span>
      </li>
      <li>
        <span>and filtered (maximum cluster length = 200, minimum density increase = 2):<br> positive strand ("*par_pos_30_2_200.bed"), negative strand ("*par_neg_30_2_200.bed ") and concatenated ("*par_30_2_200.bed")</span>
        <ul style="list-style-type: square;">
          <li><span>Column 1: Sequence name</span></li>
          <li><span>Column 2: Strand</span></li>
          <li><span>Column 3: First position in the cluster</span></li>
          <li><span>Column 4: Last position in the cluster</span></li>
          <li><span>Column 5: Number of positions with data in the cluster</span></li>
          <li><span>Column 6: Sum of the data values in the cluster</span></li>
          <li><span>Column 7: Cluster's "minimum density"</span></li>
          <li><span>Column 8: Cluster's "maximum density"</span></li>
        </ul>
      </li>
    </ul>
  </li>
  <li style="margin-left: 42.0px;">
    <span>Combined paraclu output, formatted , in bed format</span>
    <span>("*par_30_2_200_bed.bed")</span>
    <br>
        <ul style="list-style-type: square;">
          <li><span>Column 1: Chromosome name</span></li>
          <li><span>Column 2: First position of the cluster</span></li>
          <li><span>Column 3: Last position of the cluster</span></li>
          <li><span>Column 4: Name of tag cluster (chromosome name/position/strand)</span></li>
          <li><span>Column 5: Strand</span></li>
        </ul>
  </li>
  <li>
    <span>The number of counts per tag cluster identified, in tsv format ("*counts.tsv")</span>
    <ul>
    <ul>
    <li>Column 1: Name of tag cluster (chromosome name/position/strand)</li>
    <li>Column 2 and + : number of reads aligned per sample</li>
    </ul>
    </ul>
  </li>
</ul>
<p>

<h3>References</h3>
1. Langmead B, Trapnell C, Pop M, Salzberg SL. <i>"Ultrafast and memory-efficient alignment of short DNA sequences to the human genome."</i> Genome Biol 10:R25.
2. Li H., Handsaker B., et al. <i>"The Sequence alignment/map (SAM) format and SAMtools."</i> Bioinformatics, 25, 2078-9.
3. Frith MC, Valen E, et al. <i>"A code for transcription initiation in mammalian genomes."</i>" Genome Res. 2008, 18: 1-12.
