#!/bin/bash -l

# CAGE-seq pipeline
# version 1.6
# Date: 2017-07-17 (last update) 
# Author: Michael DONG
# email: michael.dong@ki.se

# Updates:
# 

# v1.6.1
# - script corrections for public release

# v1.6.0
# - pipeline outputs changed (DNAnexus app only)

# v1.5
# - Corrected the zenbufile output to match bigWig format
# - removing of first G found in first position of reads.
# - Quality control implemented
# - changing clustering methods, from Paraclu to system based on peaks (CAGEr) - will be implemented for version 2.0

# v1.3
# - Converted raw script to DNAnexus application
# - Can now take multiple files as inputs 
# - merged three steps into one single loop (one single application for DNAnexus) 

# v1.1
# - Added the raw tag count per cluster step (bedIctss.sh) : intersecting CTSS files with the tag clusters genomic regions (paraclu output).

# v1.0
# - Initial script, test version. 

# Description:
# This pipeline pre-processes the CAGE-seq data to produce outputs that can be used for downstream analysis.
# it consists of different general steps:
# - sequence quality filtering / trimming (FastQC),
# - mapping to the reference genome (Bowtie), 
# - file format conversion (samtools/bedtools) 
# - and tag clustering (Paraclu-9)

# The pipeline also exists in DNAnexus version (See README of the repository)

# How to use:
# To run the pipeline use the following command from your Linux terminal:

# You need to indicate:
# - the input directory, where ALL and ONLY the CAGE samples you want to process (in .fastq.gz format) are located
# - the output directory, where all the results will be placed once the pipeline has finished to process
# - the filepath to your reference genome, in fastq.gz format
#
# ./main.sh </inputCAGE_directory_pathway> </output_directory_pathway> <genome_pathway>
# Example:
# ./main.sh /Users/usrname/CAGEdata/test_data /Users/usrname/pipeline_output /Users/usrname/CAGEdata/referenceEnsembl_Danio_rerio.GRCz10.fastq.gz


echo -n "Time started: " 
date

ls test_data/input/*/*.fastq | awk 'BEGIN {FS="/"}; {print $NF}' > list.txt

# Directories
IN_DIR=$1
OUT_DIR=$2
echo "Input directory: "
echo $IN_DIR
echo "Output directory: "
echo $OUT_DIR

# Reference genome to use:
REF_NAME="Danio_rerio.GRCz10 "
echo "Reference genome: "
echo $REF_NAME
REF_GENOME=$3


while read biosample_input
do
	# Start of the loop:

	# reading input names
	IFS=" " read var1 <<< ${biosample_input}

	# Initializing parameters
	echo "Currently running"
	
	echo "Input file:" ${var1}
	INP_FILE=${var1}
	OUTFILE="out_"INP_FILE"_vs_"$REF_NAME

	# Launch mapping on the samples VS the whole genome, quality filtering  
	echo -n "Time started SCR_DR_mapping_v0.sh: " 
	date
	src/./SCR_DR_mapping_v0.sh $INP_FILE $OUT_DIR $REF_GENOME $OUTFILE

	# Convert SAM output files to BED format 
	echo -n "Time started sam_To_BED.sh : " 
	date
	src/./samToBED.sh $OUTFILE $OUT_DIR

	# Convert BED files to CTSS counts BED6 format for ZENBU genome browser upload
	echo -n "Time started BedToZenbu.sh : " 
	date
	src/./bedToZenbu.sh $OUTFILE $OUT_DIR

done <$3

### part where we gather all the data

# Concatenate all the CTSS files into a unique CTSS file 
echo -n "Time started ctssToCtssAll.sh : " 
date
/src/./ctssToCtssAll.sh $DIR_OUT $DIR_OUT

# Obtaining the tag clusters from the general CTSS file obtained above
echo -n "Time started ctssToParaclu.sh : " 
date
/src/./ctssToParaclu.sh $DIR_OUT $DIR_OUT

# Intersecting individual samples CTSS files with the tag clusters genomic regions to obtain a raw tag count per cluster.
echo -n "Time started bedI.sh : " 
date
/src/./bedI.sh $DIR_OUT $DIR_OUT

echo -n "Time ended: " 
date
echo
echo "Output in $PsariOutput.gz" 
echo "END"
echo "--------------"
echo 