#!/bin/bash

echo "Based on the combined files of danio"
DIR_PAR='/proj/b2013050/Michael/CAGE_pipeline/paraclu-9'
DIR_DATA=$1
DIR_OUT=$2
mfile=ctssAll_danio.bed

echo "Filter ctss < 0.2TPM"
allctss=$(awk 'FNR==NR{sum+=$5} END {print sum}' $DIR_DATA/$mfile)
awk -v ctssall=$allctss '{if (($5/ctssall)*1000000>=0.1) print}' $DIR_DATA/$mfile > $DIR_OUT/ctssAll_01_tpm_new.bed

echo "BED into minus and positive"
sed '/+/d' $DIR_OUT/ctssAll_01_tpm_new.bed > $DIR_OUT/allm_4Z.bed
grep + $DIR_OUT/ctssAll_01_tpm_new.bed > $DIR_OUT/allp_4Z.bed


echo "CTSS output to paraclu format"
awk -F "\t" '{print$1"\t"$6"\t"$2"\t"$5}' < $DIR_OUT/allp_4Z.bed > $DIR_OUT/ctss_pos_4P
awk -F "\t" '{print$1"\t"$6"\t"$3"\t"$5}' < $DIR_OUT/allm_4Z.bed > $DIR_OUT/ctss_neg_4P

echo "Sorting the pos prior paraclu clustering"
sort -k1,1 -k3n $DIR_OUT/ctss_pos_4P > $DIR_OUT/ctss_pos_4Ps

echo "Sorting the neg prior paraclu clustering"
sort -k1,1 -k3n $DIR_OUT/ctss_neg_4P > $DIR_OUT/ctss_neg_4Ps

echo "Paraclu clustering"
$DIR_PAR/./paraclu 30 $DIR_OUT/ctss_pos_4P > $DIR_OUT/par_pos_30
$DIR_PAR/./paraclu 30 $DIR_OUT/ctss_neg_4Ps > $DIR_OUT/par_neg_30

echo "Paraclu-cut"
$DIR_PAR/./paraclu-cut.sh -d 2 -l 200  $DIR_OUT/par_pos_30 > $DIR_OUT/par_pos_30_2_200
$DIR_PAR/./paraclu-cut.sh -d 2 -l 200  $DIR_OUT/par_neg_30 > $DIR_OUT/par_neg_30_2_200

echo "Combined clusters and create BED file"
cat $DIR_OUT/par_pos_30_2_200 $DIR_OUT/par_neg_30_2_200 > $DIR_OUT/par_30_2_200
awk -F "\t" '{print$1"\t"$3"\t"$4"\t"$1":"$3".."$4","$2"\t"$6"\t"$2}' < $DIR_OUT/par_30_2_200 > $DIR_OUT/par_30_2_200.bed

echo "No of clusters paraclu 30: positive strand"
awk '{print$1}' < $DIR_OUT/par_pos_30 | wc -l

echo "No of clusters paraclu 30: negative strand"
awk '{print$1}' < $DIR_OUT/par_neg_30 | wc -l

echo "No of clusters paraclu 30 2 200: positive strand"
awk '{print$1}' < $DIR_OUT/par_pos_30_2_200 | wc -l

echo "No of clusters paraclu 30 2 200: negative strand"
awk '{print$1}' < $DIR_OUT/par_neg_30_2_200 | wc -l

echo "DONE"
