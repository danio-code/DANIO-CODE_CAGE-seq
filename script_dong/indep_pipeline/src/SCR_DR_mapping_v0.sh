#! /bin/bash -l

# SCR_DR_mapping_v0.sh
# Last edited 18-07-2017

# requires following bioinformatic tools
# - Fastx 
# - bowtie/1.1.2

# SCR_DR_mapping_v0.sh
# script for CAGE data mapping against reference genome using Bowtie application
# Includes:
# - quality filtering by fastq_quality_trimmer
# - 1st G trimming by fastx
# - creation of a bowtie index from reference genome
# - mapping against bowtie index
#
# To run the script use the following command:
# $ ./SCR_DR_mapping_v0.sh <input_directory> <output_directory> <genome_filepath>

# Updates:

# v0 - Version Zero (Used since CAGE_pipeline_v1.6, 17-07-17)
# standardized version of the script, for public use 
# all specificities to the DANIO-CODE project were removed.

echo 'CAGE mapping pipeline'
echo 'Updated for test on CAGE data for developmental time course'

# Details of the genome
SEQ_FILE="Danio_rerio.GRCz10.dna.toplevel.fa"
SEQ_NAME="Danio_rerio.GRCz10"

# CAGE data
INPUT_FILE=$1
OUTPUT_DIR=$2
GENOME_PATH=$3
SAMPLE_NAME= basename $INPUT_FILE


echo Description
echo Mapping against the reference genome
echo IN PROGRESS...
echo $SAMPLE_NAME

echo Quality filtering
fastq_quality_filter -Q 33 -q 30 -p 50 -i $INPUT_FILE > $OUTPUT_DIR/$SAMPLE_NAME".qfilter"

echo Trimming out nucleotides/linkers 
fastx_trimmer -Q33 -f 1 -l 36 -i $DIR_OUT/$SAMPLE_NAME".qfilter" > $OUTPUT_DIR/$SAMPLE_NAME".qfilter_trim"

echo Indexing reference genome with bowtie-index
mkdir $OUTPUT_DIR/bowtie_index
bowtie-index $GENOME_PATH $OUTPUT_DIR/bowtie_index/ebwt_out_db

echo Mapping against Danio rerio genome
bowtie -S -t --best -k 1 -y -p 6 --phred33-quals --chunkmbs 64 -n 2 -l 28 -e 70 $OUTPUT_DIR/bowtie_index/ebwt_out_db -q $INPUT_FILE --un $DIR_OUT"/out_"$SAMPLE_NAME"_vs_"$SEQ_NAME"_noalign" > $OUTPUT_DIR/outfile.map

