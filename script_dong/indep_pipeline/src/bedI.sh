#!/bin/bash

# bedIctss.sh
# version 1.2.0
# last edited: 27-07-17
# author: Michael DONG

for mfile in $filelist
do 
  
    STRING="File in progress: "${mfile}
    echo $STRING

    echo ${mfile}
    
    STRING="intersectBED in progress"
	echo $STRING
	bedtools intersect -a par_30_2_200_bed -b filelist-$i -loj -s > ${mfile}_counts_fortab
	echo "CTSS intersectBED done"

	echo "file processing"
	bedtools groupby -i ${mfile}_counts_fortab -g 1,2,3,4,6 -c 11 -o sum > ${mfile}_counts1_fortab
	awk -v OFS='\t' '{if($6=="-1") $6=0; print $6 }' ${mfile}_counts1_fortab > $DIR_BEDICTSS/${mfile}_counts

    rm ${mfile}_counts_fortab
    rm ${mfile}_counts1_fortab
    
    name=${mfile}

    printf "\t%s" "$name" >> counts.tsv
	echo "Done"

done

echo "Value of par_30_2_200_bed: '$par_30_2_200_bed'"

mkdir bedictss
DIR_BEDICTSS="bedictss"

printf "%s" "coordinates" > counts.tsv

echo "Creating the count matrix"
printf "\n" >> counts.tsv
awk '{ print $4}' par_30_2_200_bed > paraclu_coordinates.bed
paste -d "\t"  paraclu_coordinates.bed $DIR_BEDICTSS/* >> counts.tsv
echo "Done"