#!/bin/bash

# samToBED.sh
# v1.0
# Last edited 18-07-2017

# requires following bioinformatic tools
# - samtools

echo Loading bioinformatics modules
module load bioinfo-tools 
module load samtools
module list
module load BEDTools/2.21.0

filename=$1
input_dir=$2

cd $2
  
STRING="File in progress: "${filename}
echo $STRING
  
echo "Converting SAM to BAM"
samtools view -Sb ${filename}.map > ${filename}.bam
echo "Filtering BAM file"

samtools view -b -q 20 ${filename}.bam -o ${filename}q.bam
echo "q20Filtered"

echo "Sorting filtered BAM file"
samtools sort ${filename}q.bam -o ${filename}s.bam
echo "SORTED"

echo "Indexing BAM file"
samtools index ${filename}s.bam
echo "INDEXED"

echo "Converting to BED"
bamToBed -i ${filename}s.bam > ${filename}.bed
echo "Converted to BED"

