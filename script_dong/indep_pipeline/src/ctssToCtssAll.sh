#!/bin/bash

# ctssToCtssAll.sh
# version 1.1.0
# Last edited : 27-07-17
# Author: Michael DONG (michael.dong@ki.se)

#bedtools required
module load bioinfo-tools 
module load  BEDTools/2.21.0

echo 'Usage: ctssToCtssAll.sh <DIR_DATA> <DIR_OUT>'

DIR_DATA=$1
DIR_OUT=$2

pCtss=ctssPooled

echo "Pooling ctss"
cat $DIR_DATA/*_ctss_4Z.bed > $DIR_OUT/$pCtss

echo "Sorting pooled ctss"
sort -k 1,1 -k 2,2n -k 3,3n -k 6,6  $DIR_OUT/$pCtss > $DIR_OUT/$pCtss.Sorted

echo "Grouping and adding ctss"
bedtools groupby -i $DIR_OUT/$pCtss.Sorted -g 1,2,3,6 -c 5 -o sum > $DIR_OUT/$pCtss.SortedGrouped

echo "Formating file"
awk -F "\t" '{print$1"\t"$2"\t"$3"\t"$1":"$2"-"$3"\t"$5"\t"$4}' < $DIR_OUT/$pCtss.SortedGrouped > $DIR_OUT/ctssAll_danio.bed
