#!/bin/bash

echo "Merging .bam files..."
cat *s.bam > all.bam

echo "All bam into bed"
samtools sort all.bam alls
samtools index alls.bam
bamToBed -i alls.bam > alls.bed

echo "BED into minus and positive"
perl divBed_02.pl

echo "CTSS counts: postive strand"
groupBy -i allp.bed -g 1,2 -c 6 -o count > ctss_pos

echo "CTSS counts: negative strand"
groupBy -i allm.bed -g 1,3 -c 6 -o count > ctss_neg

echo "CTSS output to paraclu format"
awk -F "\t" '{print$1"\t""+""\t"$2"\t"$3}' < ctss_pos > ctss_pos_4P
awk -F "\t" '{print$1"\t""-""\t"$2"\t"$3}' < ctss_neg > ctss_neg_4P

echo "Sorting the neg prior paraclu clustering"
sort -k3n ctss_neg_4P > ctss_neg_4Ps

echo "Paraclu clustering"
~olga/glob/tools/paraclu-8/./paraclu 30 ctss_pos_4P > par_pos_30
~olga/glob/tools/paraclu-8/./paraclu 30 ctss_neg_4Ps > par_neg_30

echo "Paraclu-cut"
~olga/glob/tools/paraclu-8/./paraclu-cut.sh 2 100 par_pos_30 > par_pos_30_2_100
~olga/glob/tools/paraclu-8/./paraclu-cut.sh 2 100 par_neg_30 > par_neg_30_2_100

echo "Combined clusters and create BED file"
cat par_pos_30_2_100 par_neg_30_2_100 > par_30_2_100
awk -F "\t" '{print$1"\t"$3"\t"$4"\t""par_30_2_100""\t"$5"\t"$2}' < par_30_2_100 > par_30_2_100.bed

echo "No of clusters paraclu 30: positive strand"
awk '{print$1}' < par_pos_30 | wc -l

echo "No of clusters paraclu 30: negative strand"
awk '{print$1}' < par_neg_30 | wc -l

echo "No of clusters paraclu 30 2 100: positive strand"
awk '{print$1}' < par_pos_30_2_100 | wc -l

echo "No of clusters paraclu 30 2 100: negative strand"
awk '{print$1}' < par_neg_30_2_100 | wc -l

echo "DONE" 
