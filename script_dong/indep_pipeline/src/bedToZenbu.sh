#!/bin/bash

# bedToZenbu.sh
# v1.0
# Last edited 18-07-2017

# requires following bioinformatic tools
# - BEDTools

mfile=$1
input_dir=$2

cd $2

echo "BED into minus and positive"
perl /proj/b2013050/Michael/CAGE_pipeline/src/divBed_01.pl ${mfile}

echo "CTSS counts: postive strand"
groupBy -i ${mfile}_p.bed -g 1,2 -c 6 -o count > ${mfile}_ctssPos

echo "CTSS counts: negative strand"
groupBy -i ${mfile}_m.bed -g 1,3 -c 6 -o count > ${mfile}_ctssNeg

echo "CTSS output to BED6 format (e.g. for Zenbu upload"
awk -F "\t" '{print"chr"$1"\t"$2"\t"$2+1"\t""chr"$1":"$2"-"$2",+""\t"$3"\t""+"}' < ${mfile}_ctssPos > ${mfile}_ctssPos_4Z.bed
awk -F "\t" '{print"chr"$1"\t"$2-1"\t"$2"\t""chr"$1":"$2"-"$2",-""\t"$3"\t""-"}' < ${mfile}_ctssNeg > ${mfile}_ctssNeg_4Z.bed

cat ${mfile}_ctssPos_4Z.bed ${mfile}_ctssNeg_4Z.bed > ${mfile}_ctss_4Z.bed

rm ${mfile}_p.bed
rm ${mfile}_m.bed
rm ${mfile}_ctssPos
rm ${mfile}_ctssNeg

rm *ctssPos_4Z.bed
rm *ctssNeg_4Z.bed
