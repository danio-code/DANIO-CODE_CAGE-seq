
$file_input=$ARGV[0].'.bed';
$file_out1='>'.$ARGV[0].'_p.bed';
$file_out2='>'.$ARGV[0].'_m.bed';

print"$file_input\n";
print"$file_out1\n";
print"$file_out2\n";

# Open files
open (fid1, $file_input) or die("Could not open the file");
open (fid2, $file_out1) or die("Could not open fid2");
open (fid3, $file_out2) or die("Could not open fid3");

# For each line
foreach $line (<fid1>) {

 #remove thecarriage return
    chomp $line;

 #split the linebetween tabs
 #and get the different elements
 ($n1,$dt1, $dt2, $n2, $qu, $strand) = split(/\t/, $line);

$x = 7;
if ($strand eq "+") {
    print fid2 "$line\n";
} else {
    print fid3 "$line\n";
}
#print"$line\n";
#print"$name\n";

}
close (fid1);
close (fid2);
close (fid3);
