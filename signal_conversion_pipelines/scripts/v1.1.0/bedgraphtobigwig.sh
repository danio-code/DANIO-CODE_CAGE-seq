#bedgraphtobigwig.sh
#v1.1.0

# Role:
# Converts the bedgraph outputs from the DANIO-CODE CAGE-seq CAGEr formatting pipeline to bigwig

# bedgraph output location
cd /Users/mido/Desktop/CAGE_GrCz11/input_181018/bedgraph

# for each file
for plus_filename in *.normalized.plus.bedGraph; do
    
    # getting the minus name and the normal one
    minus_filename=${plus_filename//.normalized.plus.bedGraph/.normalized.minus.bedGraph}
    filename=${plus_filename//.normalized.plus.bedGraph/.normalized.bedGraph}
    new_filename=${filename//.bedGraph/_sorted_duplicatesremoved.bigWig}
    
    # removing headers
    tail -n +2 "$plus_filename" > "$plus_filename.tmp" && mv "$plus_filename.tmp" "$plus_filename"
    tail -n +2 "$minus_filename" > "$minus_filename.tmp" && mv "$minus_filename.tmp" "$minus_filename"

    # concatenation
    cat $plus_filename $minus_filename > $filename

	# sorting, remove duplicates
    sort -k1,1 -k2,2n "$filename" | awk '!seen[$2]++' > "$filename.sort" #&& mv "$filename.tmp" "$filename"
    
    # clip values : removing off-chromosome/absurd position values
    ./bedClip "$filename.sort" danRer11.chrom.sizes "$filename.clip"
    
    # Convert to bigWig:
    ./bedGraphToBigWig "$filename.clip" danRer11.chrom.sizes "$new_filename"
    
done



