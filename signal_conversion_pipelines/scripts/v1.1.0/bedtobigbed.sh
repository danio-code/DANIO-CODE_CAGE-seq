#bedtobigbed.sh
#v1.1.0

# Role:
# Converts the bed outputs from the DANIO-CODE CAGE-seq pipeline to bigbed

# indicate directory with tag clustering outputs
cd /Users/mido/Desktop/CAGE_GrCz11/input_181018/tagclusters

# for each file
for filename in *.tagClusters.bed; do

	# removing header, sorting
    tail -n +2 "$filename" | sort -k1,1 -k2,2n > "$filename.sort" #&& mv "$filename.tmp" "$filename"
    
    # naming the new file (replacing extension)
    new_filename=${filename//.bed/_sorted.bb}
    
    # clip values
    ./bedClip "$filename.sort" danRer11.chrom.sizes "$filename.clip"
    
    # Convert to bigBed:
    ./bedToBigBed "$filename.clip" danRer11.chrom.sizes "$new_filename"
    
done