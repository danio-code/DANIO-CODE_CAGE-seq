#bedtobigBed.sh
#v1.0.0

# removing header if there is one
tail -n +2 DCD0001513SQ.tagClusters.bed > s18.tagClusters_nohead.bed

# Convert to ensembl nomenclatures:
sed 's/^chr//g' s18.tagClusters_nohead.bed | sed '/KN\d*/s/\.1/v1/g' | sed '/KN\d*/s/^/Un_/g' | sed 's/^/chr/g' | sed 's/^chrMT/chrM/g' > s18.tagClusters_nohead_ensemblID.bed

# Sort the file
sort -k1,1 -k2,2n s18.tagClusters_nohead_ensemblID.bed > s18.tagClusters_nohead_ensemblID_sorted.bed

# Convert to bigBed:
bedToBigBed s18.tagClusters_nohead_ensemblID_sorted.bed danRer10.chrom.sizes.txt s18.tagClusters_nohead_ensemblID_sorted.bb



