#bedGraphtobigWig.sh
#v1.0.0

#concatenate plus and minus output file
cat s18.CTSS.normalized.plus.bedGraph s18.CTSS.normalized.minus.bedGraph > s18.CTSS.normalized.bedGraph

# Convert to ensembl nomenclatures:
sed 's/^chr//g' s18.CTSS.normalized.bedGraph | sed '/KN\d*/s/\.1\t/v1\t/g' | sed '/KN\d*/s/^/Un_/g' | sed 's/^/chr/g' | sed 's/^chrMT/chrM/g' > s18.CTSS.normalized_ensemblIDs.bedGraph

# Sort the file
sort -k1,1 -k2,2n s18.CTSS.normalized_ensemblIDs.bedGraph > s18.CTSS.normalized_ensemblIDs_sorted.bedGraph

# removing duplicated positions
awk '!seen[$2]++' s18.CTSS.normalized_ensemblIDs_sorted.bedGraph > s18.CTSS.normalized_ensemblIDs_sorted_duplicatesremoved.bedGraph

# removing header if there is one
tail -n +2 s18.CTSS.normalized_ensemblIDs_sorted_duplicatesremoved.bedGraph > s18.CTSS.normalized_ensemblIDs_sorted_duplicatesremoved_noheader.bedGraph

# removing off-chromosome/absurd position values
bedClip s18.CTSS.normalized_ensemblIDs_sorted_duplicatesremoved_noheader.bedGraph danRer10.chrom.sizes.txt s18.CTSS.normalized_ensemblIDs_sorted_duplicatesremoved_clip.bedGraph

# Convert to bigwig:
bedGraphToBigWig s18.CTSS.normalized_ensemblIDs_sorted_duplicatesremoved_clip.bedGraph http://hgdownload.cse.ucsc.edu/goldenPath/danRer10/bigZips/danRer10.chrom.sizes s18.CTSS.normalized_ensemblIDs_sorted_duplicatesremoved.bigWig
