<h1>DANIO-CODE CAGE-seq signal conversion pipeline:</h1>

<ul>
This repository contains the scripts used for the conversion of processing pipeline outputs 
to bigbed and bigWig UCSC format. ("<a href="https://gitlab.com/danio-code/DANIO-CODE_CAGESeq/tree/master/script_dong/signal_conversion_pipeline">/script_dong/signal_conversion_pipeline</a>"), </li>
</ul>

<h2>Updates</h2>

<ul>
  <li>CAGE-seq signal conversion workflow</li>
</ul>

<h3>v1.1.0</h3>
<ul>
  <li> Changed clustering method from distclu to paraclu. </li>
</ul>

<h3>v1.0.0</h3>
<ul>
  <li>Initial release. </li>
</ul>

<h2>How to run the pipeline</h2>

<h4>Dependencies (to install, prior to running the pipeline):</h4>
*   bash
*   bedtools
*   R

<h4>Inputs: </h4>
*   bed output from CAGE pipeline

<h2>Description of the pipeline</h2>

<p>
The pipeline converts the bed mapping outputs from the CAGE-seq pipeline to the specific bigWig and bigBed format for UCSC track.

</p>

<h3>Outputs</h3>

<p>
  <span> For each sample: </span>
</p>
<ul style="list-style-type: square;">
    <li>
    bigWig file. For more information : <a href="https://genome.ucsc.edu/goldenpath/help/bigWig.html">UCSC bigWig format</a>
    </li>
    <li>
    bigBed file: For more information : <a href="https://genome.ucsc.edu/goldenpath/help/bigBed.html">UCSC bigBed format</a>
    </li>
</ul>


<h4>Flow of the pipeline</h4>

```
CAGE-seq_signal_conversion_pipeline_v1.0

INPUTS:      *.VS.genome.bed          *.bedGraph           *.VS.genome.bed
            (CAGE pipeline v1.6)      chrom.sizes            chrom.sizes
                  |                       |                       |
                  |                       |                       |
                  V                       V                       V 
STEPS:        CAGEr_proc0.R       bedGraphtobigWig.sh       bedTobigBed.sh
                  |                       |                       |
                  |                       |                       |
                  |                       |                       |
                  V                       V                       V                     
OUTPUTS:       *.bedGraph              *.bigWig                *.bigBed   
```
